#!/usr/bin/env bash
#
# Create a compressed tarfile for the MuST data-hub
#
set -e
type git zig
vers=$(git describe --tags --always --dirty --match=v* 2> /dev/null || \
	   echo v0)
VERSION="${vers#v}"
PKG="must-ntripclient_${VERSION}"
DESTDIR="$(pwd)/dist/$PKG"

rm -rf dist
make bindir= DESTDIR=$DESTDIR -C src install
cp -av systemd $DESTDIR
[[ -d scripts ]] && cp -av scripts $DESTDIR
cd dist && tar -c -z -f "${PKG}.tar.gz" "$PKG"
