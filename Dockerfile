FROM alpine:latest as build

RUN apk update && apk add --no-cache \
  build-base binutils gcc libgcc linux-headers make musl-dev libc-dev
COPY ./src /src
WORKDIR /src
RUN make ntripclient

FROM alpine:latest

RUN apk update && apk add --no-cache socat bash
COPY --from=build /src/ntripclient /usr/bin/ntripclient
COPY entrypoint.sh /

ENTRYPOINT ["/entrypoint.sh"]

