#!/bin/bash

: ${RTK_SERIAL_ADDR=192.9.0.12:4002}
: ${NTRIP_MOUNTPOINT=SSHO3}
: ${NTRIP_PORT=8080}
: ${NTRIP_HOST=156.74.250.108}

if [[ -e $RTK_SERIAL_ADDR ]]; then
    # We are using an actual serial port
    device=$RTK_SERIAL_ADDR
else
    # Setup a virtual serial port for the RTK messages
    socat pty,link=/dev/rtk,rawer,wait-slave TCP4:${RTK_SERIAL_ADDR},forever &
    device=/dev/rtk
fi

if [[ -n $1 ]]; then
    exec "$@"
else
    # When using a virtual serial port, we need to allow time
    # for the pseudo-tty to be created by socat.
    n=10
    until [[ -e $device ]] || ((n == 0)); do
        sleep 1
        ((n--))
    done

    [[ -e $device ]] || {
        echo "Cannot access GPS @ $RTK_SERIAL_ADDR" 1>&2
        exit 1
    }

    logopt=()
    [[ -n $NTRIP_LOG ]] && {
        mkdir -p /logs
        logopt=("-l" "/logs/$NTRIP_LOG")
    }

    exec ntripclient -m $NTRIP_MOUNTPOINT \
         -u $NTRIP_USER -p $NTRIP_PASSWORD \
         -r $NTRIP_PORT -s $NTRIP_HOST \
         "${logopt[@]}" \
         -D $device -B 19200
fi
