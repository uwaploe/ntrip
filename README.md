# MuST NTRIP Client

This repository is used to build a version of [ntripclient](https://igs.bkg.bund.de/root_ftp/NTRIP/software/ntripclient.zip) to run as a user-mode Systemd service on the MuST data-hub. Ntripclient provides RTK correction data for the MuST shipboard GPS unit.

## Installing

Download a compressed tarfile of the most recent release from the Downloads section of this repository to the `~sysop/stow` directory on the data-hub. Unpack the archive, run the installation script, and restart the service (run the commands below from the `sysop` account):

``` shell
cd ~/stow/must-ntripclient_$VERSION
./scripts/install.sh
systemctl --user restart must-ntrip.service
```

## Environment Variables

| **Name**         | **Description**                       | **Default Value** |
|------------------|---------------------------------------|-------------------|
| RTK_SERIAL_DEV   | Serial device for RTK output          | /dev/ttyUSB1      |
| NTRIP_USER       | Ntrip user name                       | -                 |
| NTRIP_PASSWORD   | Ntrip account password                | -                 |
| NTRIP_MOUNTPOINT | Ntrip mount-point (reference station) | -                 |
| NTRIP_PORT       | Ntrip server TCP port                 | 8080              |
| NTRIP_HOST       | Ntrip server IP address               | 156.74.250.108    |

The environment variables are read from the file `~sysop/config/ntrip.env`.
